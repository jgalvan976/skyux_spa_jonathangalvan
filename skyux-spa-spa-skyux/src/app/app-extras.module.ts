import {
  NgModule
} from '@angular/core';

import {
  AppSkyModule
} from './app-sky.module';

import {
  SkyAutonumericModule
} from '@skyux/autonumeric';

import {
  SkyDocsToolsModule,
  SkyDocsToolsOptions
} from '@skyux/docs-tools';

import {
  SkyDataManagerModule
} from '@skyux/data-manager';

import {
  SkyDropdownModule
} from '@skyux/popovers';

import {
  SkyInfiniteScrollModule,
  SkyRepeaterModule
} from '@skyux/lists';

import {
  SkyBackToTopModule,
  SkyToolbarModule
} from '@skyux/layout';

import {
  SkySearchModule
} from '@skyux/lookup';

import {
  SkyModalModule
} from '@skyux/modals';

import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  imports:[
    AgGridModule.withComponents([])
  ],
  exports: [
    AppSkyModule,
    AgGridModule,
    SkyAutonumericModule,
    SkyDataManagerModule,
    SkyDocsToolsModule,
    SkyDropdownModule,
    SkyInfiniteScrollModule,
    SkyModalModule,
    SkyRepeaterModule,
    SkySearchModule,
    SkyToolbarModule,
    SkyBackToTopModule
  ],providers: [
    {
      provide: SkyDocsToolsOptions
    }
  ],
})
export class AppExtrasModule { }
