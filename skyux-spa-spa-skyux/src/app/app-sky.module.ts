import {
  NgModule
} from '@angular/core';

import {
  SkyAvatarModule
} from '@skyux/avatar';

import {
  SkyAlertModule,
  SkyKeyInfoModule
} from '@skyux/indicators';

import {
  SkyFluidGridModule
} from '@skyux/layout';

import {
  SkyNavbarModule
} from '@skyux/navbar';

import {
  SkyIdModule
} from '@skyux/core';

import {
  SkyDatepickerModule
} from '@skyux/datetime';

import {
  SkyInputBoxModule
} from '@skyux/forms';

import{
  SkyEmailValidationModule
} from '@skyux/validation';

import {
  SkyAgGridModule
} from '@skyux/ag-grid';

@NgModule({
  exports: [
    SkyAvatarModule,
    SkyAlertModule,
    SkyKeyInfoModule,
    SkyFluidGridModule,
    SkyNavbarModule,
    SkyDatepickerModule,
    SkyIdModule,
    SkyInputBoxModule,
    SkyEmailValidationModule,
    SkyAgGridModule,
  ]
})
export class AppSkyModule { }
